# Python TDD

Ensure you have the Microsoft Python extension installed if using VS Code.  You may also need to run VS Code form this directory, not the one_file_tdd_examples.

To run the tests type:

```
python3 test.py
```

To run the main file directly you can type:

```
python3 main.py
```



