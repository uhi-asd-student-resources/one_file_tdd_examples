def hello_world():
    return "hello world"

def hello_tom():
    return "hello tom"

def is_even(n):
    return n % 2 == 0

if __name__ == "__main__":
    print(hello_world())
    print(hello_tom())