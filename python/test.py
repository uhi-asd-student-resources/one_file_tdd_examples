import unittest
from main import *


class MyTests(unittest.TestCase):
    def test_hello(self):
        self.assertEqual(hello_world(), "hello world")

    def test_hello_tom(self):
        self.assertEqual(hello_tom(), "hello tom")

    def check_number_is_divisible_by_2_input_10(self):
        self.assertTrue(is_even(10))

    def check_number_is_divisible_by_2_input_20(self):
        self.assertTrue(is_even(20))


if __name__ == "__main__":
    unittest.main()
