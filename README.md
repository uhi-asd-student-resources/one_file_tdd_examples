# One file examples in a variety of languages

Using TDD examples to solve problems.

Watch [Uncle Bob](https://www.youtube.com/watch?v=58jGpV2Cg50&t=16s) explain about TDD.  He does his example in Java but you can do TDD in any language.  The principles are the same:

1. Write the simplest test you can.
2. Run test, it will fail - this is good.
3. Write minimum code to pass test.
4. Run test, it should pass.
5. Go back to 1 and write another test.

In principle, if you are following the practices of Test Driven Development (TDD) **NO** code is written without at least 1 test that will execute that line of code.

In this repo, each example has the following:

1. A file to put your code in.
2. A file to put the test code in.
3. They are joined by a Test Framework.  Most languages have one and it should be one of the first things you seek out when learning a new language.