package main
import "testing"

func TestHelloWorld(t *testing.T) {
    got := hello_world()
    if got != "hello world" {
        t.Errorf("hello_world() = %s; want 'hello world'", got)
    }
}

