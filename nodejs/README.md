# Node.js TDD

* Install node.js and npm (https://nodejs.org/en/download/)
* Install mocha by doing the following in the project directory

```
npm install mocha
```

* Run tests

```
npm test
```

* What's where?
  
- app.js contains the code of your solution
- test.js contains your test code
- package.json contains the npm instructions
- package-lock.json contains the module setup