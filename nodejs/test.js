var assert = require('assert');
var app = require('./app')

describe('Example', function () {
  describe('hello_world()', function () {
    it('should return "hello world" when called', function () {
      assert.strictEqual(app.hello_world(), "hello world");
    });
  });
});
