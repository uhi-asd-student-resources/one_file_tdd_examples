import org.junit.Test;
import static org.junit.Assert.*;

public class HelloWorldTest {

    @Test
    public void testHelloworld() {
        HelloWorld my_test = new HelloWorld();

        String result = my_test.hello_world();

        assertEquals("hello world", result);

    }
}