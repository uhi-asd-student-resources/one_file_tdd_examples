# Java example

In general it is easier to use an IDE like Netbeans to create and manage Java projects.  However, this simple program shows that this is not required for smaller programs.

**This assumes you have OpenJDK or similar installed.**


Compile and run the program directly 
```
$ javac main.java
$ java Helloworld
```

To run the tests you need to add some environment variables:

```
$ export JUNIT_HOME=$(pwd)
$ export CLASSPATH=$CLASSPATH:$JUNIT_HOME/junit-4.13.jar:$JUNIT_HOME/hamcrest-core-1.3.jar:.
$ javac TestRunner.java HelloWorldTest.java HelloWorld.java
$ java TestRunner
```
