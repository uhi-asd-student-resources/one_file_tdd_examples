#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "helloworld.hpp"

// see https://github.com/catchorg/Catch2/blob/master/docs/tutorial.md#writing-tests
// for tutorial on how to use this

TEST_CASE("check hello world", "[basic]") {
    REQUIRE( hello_world() == "hello world" );
}
