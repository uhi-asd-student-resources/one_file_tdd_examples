# Getting started in C++ for one file programs

* Install cmake following the instructions for your OS at https://cmake.org/install/


* Download [catch2.hpp](https://raw.githubusercontent.com/catchorg/Catch2/master/single_include/catch2/catch.hpp), learn more about catch2.hpp at https://github.com/catchorg/Catch2/blob/master/docs/tutorial.md#writing-tests


# Get the test running

```
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ./program
$ ./test
```

# What's where?

* main.cpp should contain your solution 
* main.hpp contains a declaration of the function with your solution
* tests.cpp contains your tests 
