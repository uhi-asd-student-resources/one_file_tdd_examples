using Xunit;

namespace HelloWorld_NS
{
    public class HelloWorldTest
    {
        [Fact]
        public void PassingTest()
        {
            string s = HelloWorld.hello_world();
            Assert.Equal("hello world", s);
        }

    }
}
