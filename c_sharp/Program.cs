﻿using System;

namespace HelloWorld_NS {
    public class HelloWorld {
        public static string hello_world() {
            return "hello world";
        }
        static void Main(string[] args) {
            Console.WriteLine(hello_world());
        }
    }
}