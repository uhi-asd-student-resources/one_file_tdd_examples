# C# Example using xUnit

Compiled on dotnet in Linux.

To run the tests type the following:

```
dotnet test
```

To run directly type:

```
dotnet run
```